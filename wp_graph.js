//main indicator graph
$(function () {
    Highcharts.setOptions({ // This is for all plots, change Date axis to local timezone
        global: {
            useUTC: false
        }
    });
    var chart;
    //getting the data here now
	var theOtherScript = 'http://dl.dropbox.com/u/90217628/data_hourly.js';
	var el = document.createElement('script');
	el.async = false;
	el.src = theOtherScript;
	el.id = 'wp';
	el.type = 'text/javascript';
	(document.getElementsByTagName('HEAD')[0]||document.body).appendChild(el);
    var chart;
    $('#wp').load(function () {
        chart = new Highcharts.Chart({
            credits: {
                enabled: true,
                text: 'Sources: IASBet & Sportsbet; Centrebet & Sportingbet; Betstar; Luxbet;Tom Waterhouse; Betfair Australia.',
                href: false
            },
            chart: {
                renderTo: 'container',
                zoomType: 'xy',
                type: 'line',
                marginLeft: 50,
                marginRight: 50,
                marginBottom: 50,
                resetZoomButton: {
                    theme: {
                        fill: 'white',
                        stroke: 'silver',
                        r: 0,
                        states: {
                            hover: {
                                fill: '#454545',
                                style: {
                                    color: 'white'
                                }
                            }
                        }
                    }
                }
            },
            title: {
                text: '2013 Federal Election',
                x: -20, //center
                style: {
                    color: '#28447B',
                    fontWeight: 'bold',
                    fontSize: '17pt'
                }
            },
            subtitle: {
                text: 'Estimated probability of victory',
                x: -20,
                style: {
                    color: '#28447B',
                    //fontWeight: 'bold',
                    fontSize: '13pt'
                }
            },
            xAxis: {
                type: 'datetime',
                minRange: 7 * 24 * 3600000, // 1 week
                dateTimeLabelFormats: {
                    second: '%H:%M:%S',
                    minute: '%H:%M',
                    hour: '%H:%M',
                    day: '%e %b',
                    week: '%e %b',
                    month: '%b \'%y',
                    year: '%Y'
                },
                //tickInterval: 24*3600*1000*90,
                //showFirstLabel: false,
                minTickInterval: 1 * 24 * 3600000, //1 day
                startOnTick: false,
                labels: {
                    style: {
                        color: '#454545',
                        //fontWeight: 'bold',
                        fontSize: '11pt'
                    }
                }
            },
            yAxis: [{
                //LHS axis
                title: {
                    text: '%',
                    align: 'high',
                    rotation: 0,
                    offset: 10,
                    style: {
                        color: '#454545',
                        //fontWeight: 'bold',
                        fontSize: '11pt'
                    }
                },
                labels: {
                    style: {
                        color: '#454545',
                        //fontWeight: 'bold',
                        fontSize: '11pt'
                    }
                },
                showLastLabel: false,
                showFirstLabel: false,
                minRange: 3,
                minTickInterval: 1,
                min: 0,
                max: 100,
                opposite: false,
                startOnTick: true,
                //tickInterval: 5,
                allowDecimals: false
            }, {
                //RHS axis
                title: {
                    text: '%',
                    align: 'high',
                    rotation: 0,
                    offset: 20,
                    style: {
                        color: '#454545',
                        //fontWeight: 'bold',
                        fontSize: '11pt'
                    }
                },
                linkedTo: 0,
                labels: {
                    style: {
                        color: '#454545',
                        //fontWeight: 'bold',
                        fontSize: '11pt'
                    }
                },
                showLastLabel: false,
                minTickInterval: 1,
                minRange: 3,
                showFirstLabel: false,
                startOnTick: true,
                min: 0,
                max: 100,
                opposite: true,
                //tickInterval: 10,
                allowDecimals: false
            }],
            tooltip: {
                shared: true,
                //crosshairs: true,
                xDateFormat: '%d-%b-%Y %l%P',
                valueSuffix: '%',
                valueDecimals: 1
                //formatter: function () {
                //  return this.x + '<br/><b>' + this.series.name + ':' + '</b>' + this.y + '%';
                //}
            },
            legend: {
                enabled: false
                //    layout: 'vertical',
                //    align: 'right',
                //    verticalAlign: 'left',
                //   x: -20,
                //   y: 10,
                //    borderWidth: 0
            },
            series: [{
                name: 'Coalition',
                color: '#063094',
                data: lnp,
                marker: {
                    enabled: false
                },
                yaxis: 0
            }, {
                name: 'ALP',
                color: '#7e1818',
                data: alp,
                marker: {
                    enabled: false
                },
                yaxis: 0
            }]
        });
    });

    // coalition button
    $('#button').click(function () {
        chart.xAxis[0].setExtremes(lnp.pop().shift() - 3600000 * 24 * 6, lnp.pop().shift() + 3600000 * 24 * 2);
        chart.yAxis[0].setExtremes(lnp.pop().pop() - 5, lnp.pop().pop() + 5);

    });


    //alp button
    $('#button2').click(function () {
        chart.xAxis[0].setExtremes(alp.pop().shift() - 3600000 * 24 * 6, alp.pop().shift() + 3600000 * 24 * 2);
        chart.yAxis[0].setExtremes(alp.pop().pop() - 5, alp.pop().pop() + 5);

    });

    //reset button
    $('#button3').click(function () {
        chart.xAxis[0].setExtremes(alp.shift().shift(), alp.pop().shift());
        chart.yAxis[0].setExtremes(0, 100);

    });

});







