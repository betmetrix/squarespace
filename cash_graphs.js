//updating graphs each months
//basically follow your nose. 
//1. change the big graph to call in the next month of data. 
//2. then copy paste one of the existing small graphs, and update to the just finished month. 
//3. Data itself is called in around middle of this file. copy paste a block from above. 

//all graphs get same format, change/add new graphs at the bottom
//in the middle add the path to the data for that month too. 
$(function () {
    // set the theme
    Highcharts.setOptions({
        colors: ['#058DC7', '#50B432', '#ED561B', '#DDDF00', '#24CBE5', '#64E572', '#FF9655', '#FFF263', '#6AF9C4'],
        global: {
            useUTC: false
        },
        chart: {
            //backgroundColor: {
            //    linearGradient: [0, 0, 500, 500],
            //    stops: [
            //        [0, 'rgb(255, 255, 255)'],
            //        [1, 'rgb(240, 240, 255)']
            //    ]
            //},
            borderWidth: 2,
            //plotBackgroundColor: 'rgba(255, 255, 255, .9)',
            //plotShadow: true,
            plotBorderWidth: 1
        },
        title: {
            text: 'RBA Cash Rate Decision',
            style: {
                color: '#28447B',
                fontWeight: 'bold',
                fontSize: '17pt'
            }
        },
        subtitle: {
            text: 'Probability',
            style: {
                color: '#28447B',
                //fontWeight: 'bold',
                fontSize: '13pt'
            }
        },
        //xAxis: {
        //gridLineWidth: 1,
        //lineColor: '#000',
        //tickColor: '#000',
        //labels: {
        //   style: {
        //       color: '#000',
        //      font: '11px Trebuchet MS, Verdana, sans-serif'
        //  }
        //},
        //title: {
        //    style: {
        //        color: '#333',
        //        fontWeight: 'bold',
        //        fontSize: '12px',
        //        fontFamily: 'Trebuchet MS, Verdana, sans-serif'

        //  }
        //  }
        //},
        //LHS AXIS
        yAxis: [{
            //alternateGridColor: null,
            //minorTickInterval: 'auto',
            //lineColor: '#000',
            //lineWidth: 1,
            //tickWidth: 1,
            //tickColor: '#000',
            labels: {
                style: {
                    color: '#454545',
                    //fontWeight: 'bold',
                    fontSize: '11pt'
                }
            },
            showFirstLabel: false,
            showLastLabel: false,
            max: 100,
            min: 0,
            minTickInterval: 5,
            //tickInterval: 25,
            allowDecimals: false,
            opposite: false,
            title: {
                text: '%',
                align: 'high',
                rotation: 0,
                offset: 10,
                style: {
                    color: '#454545',
                    //fontWeight: 'bold',
                    fontSize: '11pt'
                }
            }
        }, { //RHS axis
            labels: {
                style: {
                    color: '#454545',
                    //fontWeight: 'bold',
                    fontSize: '11pt'
                }
            },
            showFirstLabel: false,
            showLastLabel: false,
            linkedTo: 0,
            //max: 125,
            //min: -25,
            allowDecimals: false,
            opposite: true,
            title: {
                text: '%',
                align: 'high',
                rotation: 0,
                offset: 20,
                style: {
                    color: '#454545',
                    //fontWeight: 'bold',
                    fontSize: '11pt'
                }
            }
        }],
        tooltip: {
            shared: false,
            //crosshairs: true,
            xDateFormat: '%A, %d-%b-%Y %l%P',
            valueSuffix: '%',
            valueDecimals: 1
            //formatter: function () {
            //  return this.x + '<br/><b>' + this.series.name + ':' + '</b>' + this.y + '%';
            //}
        },
        legend: {
            itemStyle: {
                //font: '9pt Trebuchet MS, Verdana, sans-serif',
                color: 'black'

            },
            itemHoverStyle: {
                color: '#039'
            },
            itemHiddenStyle: {
                color: 'gray'
            }
        },
        credits: {
            style: {
                right: '10px'
            }
        },
        labels: {
            style: {
                color: '#99b'
            }
        }
    });


    //getting the data here now
    var theOtherScript = 'https://dl.dropboxusercontent.com/u/90217628/cash_apr13.js';
    var el = document.createElement('script');
    el.async = false;
    el.src = theOtherScript;
    el.id = 'apr13data';
    el.type = 'text/javascript';
    (document.getElementsByTagName('HEAD')[0] || document.body).appendChild(el);

    var theOtherScript = 'https://dl.dropboxusercontent.com/u/90217628/cash_may13.js';
    var el = document.createElement('script');
    el.async = false;
    el.src = theOtherScript;
    el.id = 'may13data';
    el.type = 'text/javascript';
    (document.getElementsByTagName('HEAD')[0] || document.body).appendChild(el);

    var theOtherScript = 'https://dl.dropboxusercontent.com/u/90217628/cash_jun13.js';
    var el = document.createElement('script');
    el.async = false;
    el.src = theOtherScript;
    el.id = 'jun13data';
    el.type = 'text/javascript';
    (document.getElementsByTagName('HEAD')[0] || document.body).appendChild(el);







    // default options
    var options = {
        chart: {
            zoomType: 'xy'
        },
        xAxis: {
            type: 'datetime',
            minRange: 2 * 24 * 3600000, // 2 days
            dateTimeLabelFormats: {
                second: '%H:%M:%S',
                minute: '%H:%M',
                hour: '%H:%M',
                day: '%e %b',
                week: '%e %b',
                month: '%b \'%y',
                year: '%Y'
            },
            //tickInterval: 24*3600*1000*90,
            //showFirstLabel: false,
            minTickInterval: (1 / 24) * 24 * 3600000, //1 hour
            startOnTick: false,
        }
    };

    $('#apr13data').load(function () {
    // create the chart
    var chart1Options = {
        credits: {
            enabled: true,
            text: 'www.BetMetrix.com',
            href: false
        },
        chart: {
            renderTo: 'apr13'
        },
        series: [{
            name: 'No change',
            data: nochg_apr13,
            yaxis: 0,
            marker: {
                enabled: false
            }
            //pointStart: Date.UTC(2010, 0, 1),
            //pointInterval: 3600 * 1000 // one hour
        }, {
            name: '25bp cut',
            data: other_apr13,
            yaxis: 0,
            marker: {
                enabled: false
            }
        }]
    };
    chart1Options = jQuery.extend(true, {}, options, chart1Options);
        var chart1 = new Highcharts.Chart(chart1Options);
        chart1.setTitle({
            text: 'April 2013 - No change'
        });
    });
    //insert chart2Options here

    $('#may13data').load(function () {
    // create the chart
    var chart1Options = {
        credits: {
            enabled: true,
            text: 'www.BetMetrix.com',
            href: false
        },
        chart: {
            renderTo: 'may13'
        },
        series: [{
            name: 'No change',
            data: nochg_may13,
            yaxis: 0,
            marker: {
                enabled: false
            }
            //pointStart: Date.UTC(2010, 0, 1),
            //pointInterval: 3600 * 1000 // one hour
        }, {
            name: '25bp cut',
            data: other_may13,
            yaxis: 0,
            marker: {
                enabled: false
            }
        }]
    };
    chart1Options = jQuery.extend(true, {}, options, chart1Options);
        var chart1 = new Highcharts.Chart(chart1Options);
        chart1.setTitle({
            text: 'May 2013 - Cut!'
        });

        //chart1.setTitle({
        //    style: {color: 'red'}
        //});


    });
    //insert chart2Options here

    
        $('#jun13data').load(function () {
    var chart2Options = {
        credits: {
            enabled: true,
            text: 'Calcs: www.BetMetrix.com; Data sources: IASBet & Sportsbet; Centrebet & Sportingbet; Luxbet; Betfair Australia.',
            href: false
        },
        chart: {
            renderTo: 'jun13'
        },
        series: [{
            name: 'No change',
            data: nochg_jun13,
            yaxis: 0,
            marker: {
                enabled: false
            }
            //pointStart: Date.UTC(2010, 0, 1),
            //pointInterval: 3600 * 1000 // one hour
        }, {
            name: '25bp cut',
            data: other_jun13,
            yaxis: 0,
            marker: {
                enabled: false
            }
        }]
    };

    chart2Options = jQuery.extend(true, {}, options, chart2Options);

        var chart2 = new Highcharts.Chart(chart2Options);
        chart2.setTitle({
            text: 'RBA Cash Rate Decision - June 2013'
        });
    });
});