//polls graph now
$(function () {
    Highcharts.setOptions({ // This is for all plots, change Date axis to local timezone
        global: {
            useUTC: false
        }
    });
    var chart;
    //getting the data here now
	var theOtherScript = 'http://dl.dropbox.com/u/90217628/polls_data.js';
	var el = document.createElement('script');
	el.async = false;
	el.src = theOtherScript;
	el.id = 'polls';
	el.type = 'text/javascript';
	(document.getElementsByTagName('HEAD')[0]||document.body).appendChild(el);
    var chart;
    $('#polls').load(function () {
        chart = new Highcharts.Chart({
            credits: {
                enabled: true,
                text: 'Poll sources: Newspoll & The Australian; The Nielsen Company; Roy Morgan Research; Essential Media Communications',
                href: false
            },
            chart: {
                renderTo: 'container1',
                zoomType: 'xy',
                type: 'line',
                //marginTop: 70,
                marginLeft: 70,
                marginRight: 70,
                marginBottom: 70,
                resetZoomButton: {
                    theme: {
                        fill: 'white',
                        stroke: 'silver',
                        r: 0,
                        states: {
                            hover: {
                                fill: '#454545',
                                style: {
                                    color: 'white'
                                }
                            }
                        }
                    }
                }
            },
            title: {
                text: 'Probability and the Polls',
                x: -20, //center
                style: {
                    color: '#28447B',
                    fontWeight: 'bold',
                    fontSize: '17pt'
                }
            },
            subtitle: {
                text: 'Chance of Coalition victory (LHS) and 2PP polling (RHS)',
                x: -20,
                style: {
                    color: '#28447B',
                    //fontWeight: 'bold',
                    fontSize: '12pt'
                }
            },
            xAxis: {
                type: 'datetime',
                minRange: 7 * 24 * 3600000, // 1 week
                dateTimeLabelFormats: {
                    second: '%H:%M:%S',
                    minute: '%H:%M',
                    hour: '%H:%M',
                    day: '%e %b',
                    week: '%e %b',
                    month: '%b \'%y',
                    year: '%Y'
                },
                //tickInterval: 24*3600*1000*90,
                //showFirstLabel: false,
                minTickInterval: 1 * 24 * 3600000, //1 day
                startOnTick: false,
                labels: {
                    style: {
                        color: '#454545',
                        //fontWeight: 'bold',
                        fontSize: '11pt'
                    }
                }
            },
            yAxis: [{
                //LHS axis
                title: {
                    text: 'Prob(%)',
                    align: 'high',
                    rotation: 0,
                    offset: 0,
                    style: {
                        color: '454545',
                        //fontWeight: 'bold',
                        fontSize: '11pt'
                    }
                },
                labels: {
                    style: {
                        color: '#454545',
                        //fontWeight: 'bold',
                        fontSize: '11pt'
                    }
                },
                showLastLabel: false,
                showFirstLabel: false,
                minRange: 10,
                minTickInterval: 2,
                min: 50,
                max: 90,
                //opposite: false,
                //startOnTick: true,
                //tickInterval: 5,
                //allowDecimals: false
            }, {
                //rhs 2nd axis
                title: {
                    text: '2PP',
                    align: 'high',
                    rotation: 0,
                    offset: 30,
                    style: {
                        color: '#454545',
                        //fontWeight: 'bold',
                        fontSize: '11pt'
                    }
                },
                labels: {
                    style: {
                        color: '#454545',
                        //fontWeight: 'bold',
                        fontSize: '11pt'
                    }
                },
                //reversed: false,
                showLastLabel: false,
                showFirstLabel: false,
                maxRange: 30,
                minRange: 5,
                minTickInterval: 1,
                min: 40,
                max: 60,
                opposite: true,
                //startOnTick: true,
                //tickInterval: 5,
                //allowDecimals: false
            }],
            tooltip: {
                //enabled: true,
                shared: false,
                xDateFormat: '%d-%b-%Y'
                //valueSuffix: '%',
                //                         valueDecimals: 1
                //formatter: function () {
                //  return this.x + '<br/><b>' + this.series.name + ':' + '</b>' + this.y + '%';
                //}
            },
            legend: {
                enabled: true
                //    layout: 'vertical',
                //    align: 'right',
                //    verticalAlign: 'left',
                //   x: -20,
                //   y: 10,
                //    borderWidth: 0
            },
            series: [{
                name: 'Probability',
                color: "black",
                data: prob,
                type: 'line',
                marker: {
                    enabled: false
                },
                yaxis: 0
            }, {
                name: 'Newspoll',
                color: "blue",
                data: newspoll,

                type: 'line',
                yAxis: 1
            }, {
                name: 'Nielsen',
                color: "red",
                data: nielsen,
                type: 'line',
                yAxis: 1
            }, {
                name: 'Morgan',
                color: "green",
                data: morgan_face,
                type: 'line',
                yAxis: 1
            }, {
                name: 'Essential',
                color: "purple",
                data: essential,
                type: 'line',
                yAxis: 1
            }]
        });
    });

});


