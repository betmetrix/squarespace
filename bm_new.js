(function() {

// Localize jQuery variable
var jQuery;


/******** Load jQuery if not present *********/
if (window.jQuery === undefined || window.jQuery.fn.jquery !== '1.7.2') {
    var script_tag = document.createElement('script');
    script_tag.setAttribute("type","text/javascript");
    script_tag.setAttribute("src",
        "http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js");
    if (script_tag.readyState) {
      script_tag.onreadystatechange = function () { // For old versions of IE
          if (this.readyState == 'complete' || this.readyState == 'loaded') {
              chainLoadHighCharts();
          }
      };
    } else {
      script_tag.onload = chainLoadHighCharts;
    }
    // Try to find the head, otherwise default to the documentElement
    (document.getElementsByTagName("head")[0] || document.documentElement).appendChild(script_tag);
} else {
    // The jQuery version on the window is the one we want to use
    jQuery = window.jQuery;
    main();
}

    function chainLoadHighCharts() {
        var Highcharts;
        /********         // Ok, /now/ dynamically load up highchart too... *********/
if (window.Highcharts === undefined) {
    var script_tag = document.createElement('script');
    script_tag.setAttribute("type","text/javascript");
    script_tag.setAttribute("src",
        "http://code.highcharts.com/2.3.5/highcharts.js");
    if (script_tag.readyState) {
      script_tag.onreadystatechange = function () { // For old versions of IE
          if (this.readyState == 'complete' || this.readyState == 'loaded') {
              scriptLoadHandler();
          }
      };
    } else {
      script_tag.onload = scriptLoadHandler;
    }
    // Try to find the head, otherwise default to the documentElement
    (document.getElementsByTagName("head")[0] || document.documentElement).appendChild(script_tag);
} else {
    scriptLoadHandler();  
  
    //script_tag.setAttribute("src","http://code.highcharts.com/modules/exporting.js");
    }
    }
    
/******** Called once jQuery has loaded ******/
function scriptLoadHandler() {
    // Restore $ and window.jQuery to their previous values and store the
    // new jQuery in our local jQuery variable
    jQuery = window.jQuery.noConflict(true);
        
    // Finally...
    // Call our main function    
    main(); 
}

/******** Our main function ********/
function main() {
        Highcharts.setOptions({ // This is for all plots, change Date axis to local timezone
        global: {
            useUTC: false
        }
    });
    var chart;
    jQuery(document).ready(function($) { 
        /******* Load CSS *******/
//        var css_link = $("<link>", { 
//            rel: "stylesheet", 
//            type: "text/css", 
//            href: "style.css" 
//        });
//        css_link.appendTo('head');          

        /******* Load HTML *******/

            chart = new Highcharts.Chart({
                 credits: {
                     enabled: true,
                     text: 'BetMetrix.com',
                     href: 'http://www.betmetrix.com'
                 },
                 chart: {
                     renderTo: 'bm-container',
                     events: {
                         click: function () {
                             window.open('http://www.betmetrix.com', '_blank')
                         },



                     },
                     backgroundColor: '#FFFFFF',
                     zoomType: 'xy',
					 resetZoomButton: {
					 relativeTo: 'chart'
					 },
                     type: 'line',
                     marginLeft: 40,
                     marginRight: 40,
                     marginBottom: 40,
                 },
                 title: {
                     text: 'Election Worm',
                     x: -5,
                     style: {
                         color: '#000000',
                         fontWeight: 'bold',
                         fontSize: '17pt'
                     }
                 },
                 subtitle: {
                     text: 'Estimated Probability of Victory',
                     x: -5,
                     style: {
                         color: '#000000',
                         //fontWeight: 'bold',
                         fontSize: '13pt'
                     }
                 },
                 xAxis: {
                     type: 'datetime',
                     minRange: 7 * 24 * 3600000, // 1 week
                     dateTimeLabelFormats: {
                         second: '%H:%M:%S',
                         minute: '%H:%M',
                         hour: '%H:%M',
                         day: '%e %b',
                         week: '%e %b',
                         month: '%b \'%y',
                         year: '%Y'
                     },
                     //max: lnp[lnp.length-1][0]+604800000,
                     //tickInterval: 24*3600*1000*120,
                     //showFirstLabel: false,
                     minTickInterval: 1 * 24 * 3600000, //1 day
                     //maxTickInterval: 1 * 24 * 3600000*365, //30 day
                     startOnTick: false,
                     labels: {
                         style: {
                             color: '#969696',
                             //fontWeight: 'bold',
                             fontSize: '11pt'
                         }
                     }
                 },
                 yAxis: [{
                     //LHS axis
                     title: {
                         text: '%',
                         align: 'high',
                         rotation: 0,
                         offset: 10,
                         style: {
                             color: '#969696',
                             //fontWeight: 'bold',
                             fontSize: '11pt'
                         }
                     },
                     labels: {
                         style: {
                             color: '#969696',
                             //fontWeight: 'bold',
                             fontSize: '11pt'
                         }
                     },
                     showLastLabel: false,
                     showFirstLabel: false,
                     minRange: 3,
                     minTickInterval: 1,
                     min: 0,
                     max: 100,
                     opposite: false,
                     startOnTick: true,
                     //tickInterval: 5,
                     allowDecimals: false
                 }, {
                     //RHS axis
                     title: {
                         text: '%',
                         align: 'high',
                         rotation: 0,
                         offset: 20,
                         style: {
                             color: '#969696',
                             //fontWeight: 'bold',
                             fontSize: '11pt'
                         }
                     },
                     linkedTo: 0,
                     labels: {
                         style: {
                             color: '#969696',
                             //fontWeight: 'bold',
                             fontSize: '11pt'
                         }
                     },
                     showLastLabel: false,
                     minTickInterval: 1,
                     minRange: 3,
                     showFirstLabel: false,
                     startOnTick: true,
                     min: 0,
                     max: 100,
                     opposite: true,
                     //tickInterval: 10,
                     allowDecimals: false
                 }],
                 tooltip: {
                     xDateFormat: '%d-%b-%Y', //'%d-%b-%Y %l%P'
                     valueSuffix: '%',
                     valueDecimals: 1
                     //formatter: function () {
                     //  return this.x + '<br/><b>' + this.series.name + ':' + '</b>' + this.y + '%';
                     //}
                 },
                 legend: {
                     enabled: false
                     //    layout: 'vertical',
                     //    align: 'right',
                     //    verticalAlign: 'left',
                     //   x: -20,
                     //   y: 10,
                     //    borderWidth: 0
                 },
                 series: [{
                     name: 'Coalition',
                     data: lnp,
                     marker: {
                         enabled: false
                     },
                     yaxis: 0
                 }, {
                     name: 'ALP',
                     data: alp,
                     marker: {
                         enabled: false
                     },
                     yaxis: 0
                 }],
                 exporting: {
                     enabled: false
                 }
             });
            
        
        
        
        
        
        
        
        
    });
}

})(); // We call our anonymous function immediately