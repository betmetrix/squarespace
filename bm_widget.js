 $(function () {
    Highcharts.setOptions({ // This is for all plots, change Date axis to local timezone
        global: {
            useUTC: false
        }
    });
    var chart;
	//getting the data here now
	var theOtherScript = 'http://dl.dropbox.com/u/90217628/data.js';
	var el = document.createElement('script');
	el.async = false;
	el.src = theOtherScript;
	el.id = 'betmetrix';
	el.type = 'text/javascript';
	(document.getElementsByTagName('HEAD')[0]||document.body).appendChild(el);
    var chart;
    $('#betmetrix').load(function () {
     chart = new Highcharts.Chart({
            credits: {
                enabled: true,
                text: 'Source: BetMetrix.com',
                href: 'http://www.betmetrix.com'
            },
            chart: {
                renderTo: 'bm-container',
				events: {
					click: function() {
						window.open('http://www.betmetrix.com','_blank') // window.location = "http://www.betmetrix.com";
                    },                    
					//load: function() {
                    //    this.renderer.image('http://dl.dropbox.com/u/2079644/logo.png', 40, 240, 143, 57)
//                            .on('click', function() {
  //                              window.open('http://www.betmetrix.com','_blank')
    //                        })
      //              .css({
        //                cursor: 'pointer'
          //          })
            //        .css({
            //            position: 'relative',
            //            "margin-left": "-90px",
            //            opacity: 0.4
            //        })
            //        .attr({
            //            zIndex: -100
            //        })
            //        .add();
             //       }
                },
                backgroundColor: '#FFFFFF',
                zoomType: 'xy',
                type: 'line',
                marginLeft: 40,
                marginRight: 40,
                marginBottom: 40,
                resetZoomButton: {
                    theme: {
                        fill: 'white',
                        stroke: 'silver',
                        r: 0,
                        states: {
                            hover: {
                                fill: '#454545',
                                style: {
                                    color: 'white'
                                }
                            }
                        }
                    }
                }
            },
            title: {
                text: 'Election Worm',
                x: -5, 
                style: {
                    color: '#000000',
                    fontWeight: 'bold',
                    fontSize: '17pt'
                }
            },
            subtitle: {
                text: 'Estimated Probability of Victory',
                x: -5, 
                style: {
                    color: '#000000',
                    //fontWeight: 'bold',
                    fontSize: '13pt'
                }
            },
            xAxis: {
                type: 'datetime',
                minRange: 7 * 24 * 3600000, // 1 week
                dateTimeLabelFormats: {
                    second: '%H:%M:%S',
                    minute: '%H:%M',
                    hour: '%H:%M',
                    day: '%e %b',
                    week: '%e %b',
                    month: '%b \'%y',
                    year: '%Y'
                },
                //tickInterval: 24*3600*1000*120,
                //showFirstLabel: false,
                minTickInterval: 1 * 24 * 3600000, //1 day
                //maxTickInterval: 1 * 24 * 3600000*365, //30 day
                startOnTick: false,
                labels: {
                    style: {
                        color: '#969696',
                        //fontWeight: 'bold',
                        fontSize: '11pt'
                    }
                }
            },
            yAxis: [{
                //LHS axis
                title: {
                    text: '%',
                    align: 'high',
                    rotation: 0,
                    offset: 10,
                    style: {
                        color: '#969696',
                        //fontWeight: 'bold',
                        fontSize: '11pt'
                    }
                },
                labels: {
                    style: {
                        color: '#969696',
                        //fontWeight: 'bold',
                        fontSize: '11pt'
                    }
                },
                showLastLabel: false,
                showFirstLabel: false,
                minRange: 3,
                minTickInterval: 1,
                min: 0,
                max: 100,
                opposite: false,
                startOnTick: true,
                //tickInterval: 5,
                allowDecimals: false
            }, {
                //RHS axis
                title: {
                    text: '%',
                    align: 'high',
                    rotation: 0,
                    offset: 20,
                    style: {
                        color: '#969696',
                        //fontWeight: 'bold',
                        fontSize: '11pt'
                    }
                },
                linkedTo: 0,
                labels: {
                    style: {
                        color: '#969696',
                        //fontWeight: 'bold',
                        fontSize: '11pt'
                    }
                },
                showLastLabel: false,
                minTickInterval: 1,
                minRange: 3,
                showFirstLabel: false,
                startOnTick: true,
                min: 0,
                max: 100,
                opposite: true,
                //tickInterval: 10,
                allowDecimals: false
            }],
            tooltip: {
                xDateFormat: '%d-%b-%Y', //'%d-%b-%Y %l%P'
                valueSuffix: '%',
                valueDecimals: 1
                //formatter: function () {
                //  return this.x + '<br/><b>' + this.series.name + ':' + '</b>' + this.y + '%';
                //}
            },
            legend: {
                enabled: false
                //    layout: 'vertical',
                //    align: 'right',
                //    verticalAlign: 'left',
                //   x: -20,
                //   y: 10,
                //    borderWidth: 0
            },
            series: [{
                name: 'Coalition',
                data: lnp,
                marker: {
                    enabled: false
                },
                yaxis: 0
            }, {
                name: 'ALP',
                data: alp,
                marker: {
                    enabled: false
                },
                yaxis: 0
            }],
            exporting: {
                enabled: false
            }
        });
    });
});